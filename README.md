# GeneralGuide

通用遮罩（高亮）引导框架

## Getting started

接入步骤：
Step 1. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories:
```java
    allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}
```
Step 2. Add the dependency
```java
    dependencies {
	        implementation 'com.gitlab.linpeixu:GeneralGuide:1.0.3'
	}
```
## 注意

框架中依赖了ConstraintLayout 2.0.4版本，若引入遮罩引导框架导致项目报错，如果是依赖冲突可在项目的主工程下的build.gradle强制设置ConstraintLayout的版本为项目中的版本，比如项目中用的ConstraintLayout为1.1.3版本，则配置如下：
```java
    dependencies {
	        constraints {
                    implementation('androidx.constraintlayout:constraintlayout') {
                        version {
                            strictly("1.1.3")
                        }
                    }
            }
	}
```
先看演示效果：  
[点击查看演示](https://gitlab.com/linpeixu/GeneralGuide/-/blob/master/%E6%BC%94%E7%A4%BA.gif)
## 使用步骤
仅需一步：在需要用到的地方调用
```java
new GuideImpl.Builder()
             .context(MainActivity.this)
             .addConfig(new GuideConfig.Builder()
                       .target(findViewById(R.id.view_tab3))
                       .tLayoutId(R.layout.layout_guide_top_against_right)
                       .gravity(TipGravity.TOP_AGAINST_RIGHT)
                       .build())
             .build()
             .execute();
```
每个高亮视图遮罩引导效果都通过一个GuideConfig来配置，这里可通过GuideConfig.Builder来构造，然后
通过addConfig添加进来，有多个GuideConfig则多次调用addConfig，如：
```java
new GuideImpl.Builder()
             .context(MainActivity.this)
             .addConfig(new GuideConfig.Builder()
                       .target(findViewById(R.id.view_tab3))
                       .tLayoutId(R.layout.layout_guide_top_against_right)
                       .gravity(TipGravity.TOP_AGAINST_RIGHT)
                       .build())
             .addConfig(new GuideConfig.Builder()
                       .target(findViewById(R.id.view_tab4))
                       .tLayoutId(R.layout.layout_guide_top_against_left)
                       .gravity(TipGravity.TOP_AGAINST_LEFT)
                       .build())
             .build()
             .execute();
```
## 说明
1、target为目标高亮视图；  
2、tLayoutId为除高亮视图之外的引导视图对应的layout文件；  
3、gravity为对齐方式，是引导视图相对于高亮视图的对齐方式，目前框架提供了16种对齐方式，详细可看TipGravity；  
4、target、tLayoutId和gravity这三个为GuideConfig的必传参数，是一个高亮引导的三要素，高亮的目标View，提示视图，还有对齐方式；  
5、onInterceptListener这个参数可设置可不设置，是用来拦截高亮视图展示出来之前的，如果设置了记得要在回调里边调chain.process()，否则不会执行下去；  
