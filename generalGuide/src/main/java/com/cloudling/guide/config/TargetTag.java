package com.cloudling.guide.config;

/**
 * 描述: 引导视图上的目标View的标识（id、tag，可通过GuideConfig设置，不设置则跟随原目标View）
 * 联系：1966353889@qq.com
 * 日期: 2022/5/10
 */
public class TargetTag {
    private int id;
    private Object tag;

    private TargetTag() {
    }

    private TargetTag(Builder builder) {
        this.id = builder.id;
        this.tag = builder.tag;
    }

    public int getId() {
        return id;
    }

    public Object getTag() {
        return tag;
    }

    public static class Builder {
        private int id;
        private Object tag;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder tag(Object tag) {
            this.tag = tag;
            return this;
        }

        public TargetTag build() {
            return new TargetTag(this);
        }
    }
}
