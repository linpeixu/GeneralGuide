package com.cloudling.guide.config;

/**
 * 描述: 引导视图中加入的提示视图相对于目标视图的对齐方式
 * 联系：1966353889@qq.com
 * 日期: 2022/5/10
 */
public enum TipGravity {
    /**
     * 左
     */
    LEFT,
    /**
     * 上
     */
    TOP,
    /**
     * 右
     */
    RIGHT,
    /**
     * 下
     */
    BOTTOM,
    /**
     * 左上
     */
    LEFT_TOP,
    /**
     * 右上
     */
    RIGHT_TOP,
    /**
     * 右下
     */
    RIGHT_BOTTOM,
    /**
     * 左下
     */
    LEFT_BOTTOM,
    /**
     * 上左（以目标视图左上角作为提示视图的左边界）
     */
    TOP_AGAINST_LEFT,
    /**
     * 上右（以目标视图右上角作为提示视图的右边界）
     */
    TOP_AGAINST_RIGHT,
    /**
     * 下左（以目标视图左下角作为提示视图的左边界）
     */
    BOTTOM_AGAINST_LEFT,
    /**
     * 下右（以目标视图右下角作为提示视图的右边界）
     */
    BOTTOM_AGAINST_RIGHT,
    /**
     * 左上（以目标视图左上角作为提示视图的上边界）
     */
    LEFT_AGAINST_TOP,
    /**
     * 左下（以目标视图左下角作为提示视图的下边界）
     */
    LEFT_AGAINST_BOTTOM,
    /**
     * 右上（以目标视图右上角作为提示视图的上边界）
     */
    RIGHT_AGAINST_TOP,
    /**
     * 右下（以目标视图右下角作为提示视图的下边界）
     */
    RIGHT_AGAINST_BOTTOM
}
