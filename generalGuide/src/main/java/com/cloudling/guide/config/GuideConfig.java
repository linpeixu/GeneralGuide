package com.cloudling.guide.config;

import android.view.View;
import android.view.ViewGroup;

/**
 * 描述: 遮罩引导配置
 * 联系：1966353889@qq.com
 * 日期: 2022/5/9
 */
public class GuideConfig {
    /**
     * 目标View（高亮）
     */
    private View target;
    /**
     * 目标View（高亮）的宽
     */
    private int width;
    /**
     * 目标View（高亮）的高
     */
    private int height;
    /**
     * 提示视图对应的layoutId
     */
    private int tLayoutId;
    /**
     * 提示视图的宽
     */
    private int tLayoutWidth;
    /**
     * 提示视图的高
     */
    private int tLayoutHeight;
    /**
     * 提示视图相对于高亮视图的对齐方式
     */
    private TipGravity gravity;
    /**
     * 引导视图上的目标View的标识（id、tag，可通过GuideConfig设置，不设置则跟随原目标View）
     */
    private TargetTag targetTag;
    /**
     * 是否点击整个屏幕响应
     */
    private boolean isClickMatch;
    /**
     * 消费点击事件的View对应的id
     */
    private int clickInterceptViewId;
    private int marginLeft;
    private int marginRight;
    private int marginTop;
    private int marginBottom;

    private GuideConfig() {
    }

    private GuideConfig(Builder builder) {
        this.target = builder.target;
        this.width = builder.width;
        this.height = builder.height;
        this.tLayoutId = builder.tLayoutId;
        this.tLayoutWidth = builder.tLayoutWidth;
        this.tLayoutHeight = builder.tLayoutHeight;
        this.gravity = builder.gravity;
        this.targetTag = new TargetTag.Builder().id(builder.id).tag(builder.tag).build();
        this.isClickMatch = builder.isClickMatch;
        this.marginLeft = builder.marginLeft;
        this.marginRight = builder.marginRight;
        this.marginTop = builder.marginTop;
        this.marginBottom = builder.marginBottom;
        this.clickInterceptViewId = builder.clickInterceptViewId;
    }

    public int getClickInterceptViewId() {
        return clickInterceptViewId;
    }

    public void setClickInterceptViewId(int clickInterceptViewId) {
        this.clickInterceptViewId = clickInterceptViewId;
    }

    public View getTarget() {
        return target;
    }

    public void setTarget(View target) {
        this.target = target;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getTLayoutId() {
        return tLayoutId;
    }

    public void setTLayoutId(int tLayoutId) {
        this.tLayoutId = tLayoutId;
    }

    public int getTLayoutWidth() {
        return tLayoutWidth;
    }

    public void setTLayoutWidth(int tLayoutWidth) {
        this.tLayoutWidth = tLayoutWidth;
    }

    public int getTLayoutHeight() {
        return tLayoutHeight;
    }

    public void setTLayoutHeight(int tLayoutHeight) {
        this.tLayoutHeight = tLayoutHeight;
    }

    public TipGravity getGravity() {
        return gravity;
    }

    public void setGravity(TipGravity gravity) {
        this.gravity = gravity;
    }

    public TargetTag getTargetTag() {
        return targetTag;
    }

    public void setTargetTag(TargetTag targetTag) {
        this.targetTag = targetTag;
    }

    public boolean isClickMatch() {
        return isClickMatch;
    }

    public void setClickMatch(boolean clickMatch) {
        isClickMatch = clickMatch;
    }

    public int getMarginLeft() {
        return marginLeft;
    }

    public void setMarginLeft(int marginLeft) {
        this.marginLeft = marginLeft;
    }

    public int getMarginRight() {
        return marginRight;
    }

    public void setMarginRight(int marginRight) {
        this.marginRight = marginRight;
    }

    public int getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(int marginTop) {
        this.marginTop = marginTop;
    }

    public int getMarginBottom() {
        return marginBottom;
    }

    public void setMarginBottom(int marginBottom) {
        this.marginBottom = marginBottom;
    }

    public static class Builder {
        private int id = View.NO_ID;
        private Object tag;
        /**
         * 目标View（高亮）
         */
        private View target;
        /**
         * 目标View（高亮）的宽
         */
        private int width;
        /**
         * 目标View（高亮）的高
         */
        private int height;
        /**
         * 提示视图对应的layoutId
         */
        private int tLayoutId;
        /**
         * 提示视图的宽
         */
        private int tLayoutWidth;
        /**
         * 消费点击事件的View对应的id
         */
        private int clickInterceptViewId;
        /**
         * 提示视图的高
         */
        private int tLayoutHeight;
        /**
         * 提示视图相对于高亮视图的对齐方式
         */
        private TipGravity gravity;
        /**
         * 是否点击整个屏幕响应执行下一步（默认为true）
         */
        private boolean isClickMatch = true;
        private int marginLeft;
        private int marginRight;
        private int marginTop;
        private int marginBottom;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder clickInterceptViewId(int clickInterceptViewId) {
            this.clickInterceptViewId = clickInterceptViewId;
            return this;
        }

        public Builder tag(Object tag) {
            this.tag = tag;
            return this;
        }

        public Builder target(View target) {
            this.target = target;
            return this;
        }

        public Builder width(int width) {
            this.width = width;
            return this;
        }

        public Builder height(int height) {
            this.height = height;
            return this;
        }

        public Builder tLayoutId(int tLayoutId) {
            this.tLayoutId = tLayoutId;
            return this;
        }

        public Builder tLayoutWidth(int tLayoutWidth) {
            this.tLayoutWidth = tLayoutWidth;
            return this;
        }

        public Builder tLayoutHeight(int tLayoutHeight) {
            this.tLayoutHeight = tLayoutHeight;
            return this;
        }

        public Builder gravity(TipGravity gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder clickMatch(boolean isClickMatch) {
            this.isClickMatch = isClickMatch;
            return this;
        }

        public Builder marginLeft(int marginLeft) {
            this.marginLeft = marginLeft;
            return this;
        }

        public Builder marginRight(int marginRight) {
            this.marginRight = marginRight;
            return this;
        }

        public Builder marginTop(int marginTop) {
            this.marginTop = marginTop;
            return this;
        }

        public Builder marginBottom(int marginBottom) {
            this.marginBottom = marginBottom;
            return this;
        }

        public GuideConfig build() {
            if (this.target != null) {
                if (this.id == View.NO_ID) {
                    this.id = this.target.getId();
                }
                if (this.tag == null) {
                    this.tag = this.target.getTag();
                }
                if (this.width == 0 && target.getMeasuredWidth() > 0) {
                    this.width = target.getMeasuredWidth();
                }
                if (this.height == 0 && target.getMeasuredHeight() > 0) {
                    this.height = target.getMeasuredHeight();
                }
            }
            if (tLayoutWidth == 0) {
                tLayoutWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
            if (tLayoutHeight == 0) {
                tLayoutHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
            }
            return new GuideConfig(this);
        }
    }
}
