package com.cloudling.guide.callback;

/**
 * 描述: 责任链控制（与OnGuideInterceptListener配合使用）
 * 联系：1966353889@qq.com
 * 日期: 2022/5/11
 */
public interface Chain {
    /**
     * 调用该方法往下执行（与OnGuideInterceptListener配合使用）
     */
    void process();
}
