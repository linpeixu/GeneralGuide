package com.cloudling.guide.callback;

import android.view.View;
import android.view.ViewGroup;

import com.cloudling.guide.config.TargetTag;

public interface OnPrepareIntercept {
    /**
     * 引导拦截（即将将引导视图添加至屏幕）
     *
     * @param parent       引导视图（包括提示视图和高亮视图）
     * @param sourceTarget 原目标View
     * @param target       引导视图上的目标View
     * @param targetTag    引导视图上的目标View的标识（id、tag，可通过GuideConfig设置，不设置则跟随原目标View）
     */
    void onPrepare(Chain chain, ViewGroup parent, View sourceTarget, View target, TargetTag targetTag);
}
