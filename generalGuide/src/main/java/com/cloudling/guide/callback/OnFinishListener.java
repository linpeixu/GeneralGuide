package com.cloudling.guide.callback;

/**
 * 描述: 引导结束监听
 * 联系：1966353889@qq.com
 * 日期: 2022/5/11
 */
public interface OnFinishListener {
    /**
     * 引导结束（添加进来的所有GuideConfig下的引导视图都从屏幕中移除）
     */
    void onFinish();
}
