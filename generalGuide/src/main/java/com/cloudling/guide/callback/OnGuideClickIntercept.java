package com.cloudling.guide.callback;

import android.view.View;
import android.view.ViewGroup;

import com.cloudling.guide.config.TargetTag;

/**
 * 描述: 点击引导页拦截
 * 联系：1966353889@qq.com
 * 日期: 2022/5/11
 */
public interface OnGuideClickIntercept {

    /**
     * 点击引导页拦截
     *
     * @param parent 引导视图（包括提示视图和高亮视图）
     * @param target 引导视图上的目标View
     */
    void onGuideClick(Chain chain, ViewGroup parent, View target);
}
