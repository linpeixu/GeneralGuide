package com.cloudling.guide.callback;

import com.cloudling.guide.config.TargetTag;

/**
 * 描述: 遮罩视图监听
 * 联系：1966353889@qq.com
 * 日期: 2022/5/11
 */
public interface OnGuideLayoutAttachStateChangeListener {
    /**
     * 引导视图被添加至窗口
     */
    void onAttachedToWindow(TargetTag targetTag);

    /**
     * 引导视图从窗口移除
     */
    void onDetachedFromWindow(TargetTag targetTag);
}
