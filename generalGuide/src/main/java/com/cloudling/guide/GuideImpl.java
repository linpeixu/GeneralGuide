package com.cloudling.guide;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.constraintlayout.widget.ConstraintLayout;

import com.cloudling.guide.callback.Chain;
import com.cloudling.guide.callback.OnGuideClickIntercept;
import com.cloudling.guide.callback.OnGuideLayoutAttachStateChangeListener;
import com.cloudling.guide.callback.OnFinishListener;
import com.cloudling.guide.callback.OnPrepareIntercept;
import com.cloudling.guide.config.GuideConfig;
import com.cloudling.guide.config.TipGravity;

import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 描述: 遮罩引导实现类
 * 联系：1966353889@qq.com
 * 日期: 2022/5/9
 */
public final class GuideImpl {
    public static final String highlightTag = "TAG_HIGHLIGHT";
    public static String guideLayoutTag = "TAG_GUIDE_LAYOUT";
    /**
     * 上下文
     */
    private WeakReference<Activity> mContext;
    /**
     * 引导拦截（即将将引导视图添加至屏幕）监听
     */
    private final OnPrepareIntercept onPrepareIntercept;
    private final OnGuideClickIntercept onGuideClickIntercept;
    private final OnGuideLayoutAttachStateChangeListener onGuideLayoutAttachStateChangeListener;
    /**
     * 引导结束监听
     */
    private final OnFinishListener onGuideFinishListener;
    /**
     * 存储GuideConfig队列
     */
    private final ConcurrentLinkedQueue<GuideConfig> mGuideQueue;
    /**
     * 是否开启严格模式（严格模式下会确保targetView计算好宽高）
     */
    private final boolean mStrictMode;
    /**
     * 遮罩背景不透明度（0~255）
     */
    private final int mAlpha;

    private GuideImpl(Builder builder) {
        if (builder.context != null) {
            mContext = new WeakReference<>(builder.context);
        }
        this.onPrepareIntercept = builder.onPrepareIntercept;
        this.onGuideClickIntercept = builder.onGuideClickIntercept;
        this.onGuideLayoutAttachStateChangeListener = builder.onGuideLayoutAttachStateChangeListener;
        this.onGuideFinishListener = builder.onGuideFinishListener;
        this.mGuideQueue = builder.mGuideQueue;
        this.mStrictMode = builder.strictMode;
        if (builder.alpha < 0) {
            builder.alpha = 0;
        } else if (builder.alpha > 255) {
            builder.alpha = 255;
        }
        this.mAlpha = builder.alpha;
    }

    /**
     * 上下文检查
     */
    private boolean checkContext() {
        return mContext != null
                && mContext.get() != null
                && !mContext.get().isFinishing()
                && !mContext.get().isDestroyed();
    }

    /**
     * 下一步（内部使用）
     */
    private void nextInternal() {
        execute();
    }

    /**
     * 创建引导视图容器
     */
    private ViewGroup createGuideLayout(GuideConfig config) {
        return createGuideLayout(mContext.get(), config);
    }

    /**
     * 创建引导视图容器
     */
    private ViewGroup createGuideLayout(Context context, GuideConfig config) {
        if (context != null) {
            ConstraintLayout viewGroup = new ConstraintLayout(context) {
                @Override
                protected void onAttachedToWindow() {
                    super.onAttachedToWindow();
                    if (onGuideLayoutAttachStateChangeListener != null) {
                        onGuideLayoutAttachStateChangeListener.onAttachedToWindow(config.getTargetTag());
                    }
                }

                @Override
                protected void onDetachedFromWindow() {
                    super.onDetachedFromWindow();
                    if (onGuideLayoutAttachStateChangeListener != null) {
                        onGuideLayoutAttachStateChangeListener.onDetachedFromWindow(config.getTargetTag());
                    }
                }
            };
            viewGroup.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ImageView highlightView = new ImageView(context);
            highlightView.setTag(highlightTag);
            if (config.getTargetTag() != null) {
                highlightView.setId(config.getTargetTag().getId());
            }
            ConstraintLayout.LayoutParams sLp = new ConstraintLayout.LayoutParams(config.getWidth(), config.getHeight());
            sLp.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID;
            sLp.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
            if (config.getWidth() > 0 && config.getHeight() > 0) {
                int[] outLocation = new int[2];
                config.getTarget().getLocationOnScreen(outLocation);
                sLp.leftMargin = outLocation[0];
                sLp.topMargin = outLocation[1];
                config.getTarget().setDrawingCacheEnabled(true);
                highlightView.setImageBitmap(config.getTarget().getDrawingCache());
            }
            viewGroup.addView(highlightView, sLp);
            if (config.getTLayoutId() != 0) {
                View tipLayout = LayoutInflater.from(context).inflate(config.getTLayoutId(), null);
                ConstraintLayout.LayoutParams tLp = new ConstraintLayout.LayoutParams(config.getTLayoutWidth(), config.getTLayoutHeight());
                TipGravity gravity = config.getGravity();
                if (gravity != null) {
                    switch (gravity) {
                        case LEFT:
                            tLp.rightToLeft = tLp.topToTop = tLp.bottomToBottom = highlightView.getId();
                            break;
                        case TOP:
                            tLp.bottomToTop = tLp.leftToLeft = tLp.rightToRight = highlightView.getId();
                            break;
                        case RIGHT:
                            tLp.leftToRight = tLp.topToTop = tLp.bottomToBottom = highlightView.getId();
                            break;
                        case BOTTOM:
                            tLp.topToBottom = tLp.leftToLeft = tLp.rightToRight = highlightView.getId();
                            break;
                        case LEFT_TOP:
                            tLp.rightToLeft = tLp.bottomToTop = highlightView.getId();
                            break;
                        case RIGHT_TOP:
                            tLp.leftToRight = tLp.bottomToTop = highlightView.getId();
                            break;
                        case LEFT_BOTTOM:
                            tLp.rightToLeft = tLp.topToBottom = highlightView.getId();
                            break;
                        case RIGHT_BOTTOM:
                            tLp.leftToRight = tLp.topToBottom = highlightView.getId();
                            break;
                        case TOP_AGAINST_LEFT:
                            tLp.leftToLeft = tLp.bottomToTop = highlightView.getId();
                            break;
                        case TOP_AGAINST_RIGHT:
                            tLp.rightToRight = tLp.bottomToTop = highlightView.getId();
                            break;
                        case BOTTOM_AGAINST_LEFT:
                            tLp.leftToLeft = tLp.topToBottom = highlightView.getId();
                            break;
                        case BOTTOM_AGAINST_RIGHT:
                            tLp.rightToRight = tLp.topToBottom = highlightView.getId();
                            break;
                        case LEFT_AGAINST_TOP:
                            tLp.rightToLeft = tLp.topToTop = highlightView.getId();
                            break;
                        case LEFT_AGAINST_BOTTOM:
                            tLp.rightToLeft = tLp.bottomToBottom = highlightView.getId();
                            break;
                        case RIGHT_AGAINST_TOP:
                            tLp.leftToRight = tLp.topToTop = highlightView.getId();
                            break;
                        case RIGHT_AGAINST_BOTTOM:
                            tLp.leftToRight = tLp.bottomToBottom = highlightView.getId();
                            break;
                    }
                }
                tLp.leftMargin = config.getMarginLeft();
                tLp.rightMargin = config.getMarginRight();
                tLp.topMargin = config.getMarginTop();
                tLp.bottomMargin = config.getMarginBottom();
                viewGroup.addView(tipLayout, tLp);
            }
            return viewGroup;
        }
        return null;
    }

    /**
     * 执行（遮罩高亮效果）
     */
    private void executeInternal(ViewGroup guideLayout, int clickInterceptViewId, boolean isClickMatch) {
        ViewGroup decorView = (ViewGroup) mContext.get().getWindow().getDecorView();
        View oldGuideLayout = decorView.findViewWithTag(guideLayoutTag);
        if (oldGuideLayout != null) {
            decorView.removeView(oldGuideLayout);
        }
        guideLayout.setBackgroundColor(Color.argb(mAlpha, 0, 0, 0));
        guideLayout.setTag(guideLayoutTag);
        guideLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*空实现*/
            }
        });
        View clickView = clickInterceptViewId != 0 ? guideLayout.findViewById(clickInterceptViewId) : (isClickMatch ? guideLayout : guideLayout.findViewWithTag(highlightTag));
        if (clickView != null) {
            clickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onGuideClickIntercept != null) {
                        onGuideClickIntercept.onGuideClick(new Chain() {
                            @Override
                            public void process() {
                                nextInternal();
                            }
                        }, guideLayout, guideLayout.findViewWithTag(highlightTag));
                    } else {
                        nextInternal();
                    }
                }
            });
        }
        decorView.addView(guideLayout);
    }

    /**
     * 开始执行（外部调用）
     */
    public void execute() {
        if (checkContext()) {
            if (mGuideQueue.isEmpty()) {
                ViewGroup decorView = (ViewGroup) mContext.get().getWindow().getDecorView();
                View oldGuideLayout = decorView.findViewWithTag(guideLayoutTag);
                if (oldGuideLayout != null) {
                    decorView.removeView(oldGuideLayout);
                }
                if (onGuideFinishListener != null) {
                    onGuideFinishListener.onFinish();
                }
            } else {
                GuideConfig config = mGuideQueue.poll();
                if (config != null) {
                    if (mStrictMode && !config.getTarget().isInLayout()) {
                        config.getTarget().post(new Runnable() {
                            @Override
                            public void run() {
                                if (config.getWidth() == 0 && config.getTarget().getMeasuredWidth() > 0) {
                                    config.setWidth(config.getTarget().getMeasuredWidth());
                                }
                                if (config.getHeight() == 0 && config.getTarget().getMeasuredHeight() > 0) {
                                    config.setHeight(config.getTarget().getMeasuredHeight());
                                }
                                ViewGroup guideLayout = createGuideLayout(config);
                                if (onPrepareIntercept != null) {
                                    onPrepareIntercept.onPrepare(new Chain() {
                                        @Override
                                        public void process() {
                                            executeInternal(guideLayout, config.getClickInterceptViewId(), config.isClickMatch());
                                        }
                                    }, guideLayout, config.getTarget(), guideLayout.findViewWithTag(highlightTag), config.getTargetTag());
                                } else {
                                    executeInternal(guideLayout, config.getClickInterceptViewId(), config.isClickMatch());
                                }
                            }
                        });
                    } else {
                        ViewGroup guideLayout = createGuideLayout(config);
                        if (onPrepareIntercept != null) {
                            onPrepareIntercept.onPrepare(new Chain() {
                                @Override
                                public void process() {
                                    executeInternal(guideLayout, config.getClickInterceptViewId(), config.isClickMatch());
                                }
                            }, guideLayout, config.getTarget(), guideLayout.findViewWithTag(highlightTag), config.getTargetTag());
                        } else {
                            executeInternal(guideLayout, config.getClickInterceptViewId(), config.isClickMatch());
                        }
                    }
                }
            }
        }
    }

    /**
     * 清理
     */
    public void clear() {
        if (checkContext()) {
            ViewGroup decorView = (ViewGroup) mContext.get().getWindow().getDecorView();
            View oldGuideLayout = decorView.findViewWithTag(guideLayoutTag);
            if (oldGuideLayout != null) {
                decorView.removeView(oldGuideLayout);
            }
            while (mGuideQueue != null && mGuideQueue.isEmpty()) {
                mGuideQueue.poll();
            }
            mContext.clear();
        }
    }

    public static class Builder {
        private Activity context;
        /**
         * 引导拦截（即将将引导视图添加至屏幕）监听
         */
        private OnPrepareIntercept onPrepareIntercept;
        private OnGuideClickIntercept onGuideClickIntercept;
        private OnGuideLayoutAttachStateChangeListener onGuideLayoutAttachStateChangeListener;
        /**
         * 引导结束监听
         */
        private OnFinishListener onGuideFinishListener;
        /**
         * 存储GuideConfig队列
         */
        private ConcurrentLinkedQueue<GuideConfig> mGuideQueue;
        /**
         * 是否开启严格模式（严格模式下会确保targetView计算好宽高），默认开启
         */
        private boolean strictMode = true;
        /**
         * 遮罩背景不透明度（0~255）
         */
        private int alpha = 127;

        /**
         * 设置上下文环境
         */
        public Builder context(Activity context) {
            this.context = context;
            return this;
        }

        /**
         * 设置引导拦截（即将将引导视图添加至屏幕）监听
         */
        public Builder onPrepareIntercept(OnPrepareIntercept onPrepareIntercept) {
            this.onPrepareIntercept = onPrepareIntercept;
            return this;
        }

        public Builder onGuideClickIntercept(OnGuideClickIntercept onGuideClickIntercept) {
            this.onGuideClickIntercept = onGuideClickIntercept;
            return this;
        }

        /**
         * 设置遮罩视图监听
         */
        public Builder onGuideLayoutAttachStateChangeListener(OnGuideLayoutAttachStateChangeListener onGuideLayoutAttachStateChangeListener) {
            this.onGuideLayoutAttachStateChangeListener = onGuideLayoutAttachStateChangeListener;
            return this;
        }

        /**
         * 设置引导结束监听
         */
        public Builder onFinishListener(OnFinishListener onGuideFinishListener) {
            this.onGuideFinishListener = onGuideFinishListener;
            return this;
        }

        /**
         * 是否开启严格模式（严格模式下会确保targetView计算好宽高）
         */
        public Builder strictMode(boolean strictMode) {
            this.strictMode = strictMode;
            return this;
        }

        /**
         * 设置遮罩背景不透明度（0~255）
         */
        public Builder alpha(int alpha) {
            this.alpha = alpha;
            return this;
        }

        /**
         * 添加引导配置
         */
        public Builder addConfig(GuideConfig config) {
            if (config != null && config.getTarget() != null) {
                if (mGuideQueue == null) {
                    mGuideQueue = new ConcurrentLinkedQueue<>();
                }
                mGuideQueue.add(config);
            }
            return this;
        }

        public GuideImpl build() {
            return new GuideImpl(this);
        }
    }
}
